/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author wilson
 */
public class Conexao {
    
    private static Connection conn = null;

    // Create database connection
    public static Connection CreateDatabaseConnection() throws SQLException {
        if (conn == null || conn.isClosed() == true) {
            try{
                String user = "wilson";
                String password = "123";
                String dburl = "jdbc:postgresql://localhost:5534/wilson";
                
                conn = DriverManager.getConnection(dburl, user, password);
                System.out.println("chegou aqui");

            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Connection database error");
                return null;
            }

        }
        return conn;

    }

    
}
