/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.calculadora;

/**
 *
 * @author wilso
 */
public interface Inte_Calc {
   public abstract double soma(double x, double y);
   public abstract double subi(double x, double y);
   public abstract double muti(double x, double y);
   public abstract  double div(double x, double y);
}
