
package com.mycompany.calculadora;

public class Met_Calc implements Inte_Calc {

    private double t;

    /**
     *
     * @param x
     * @param y
     * @return
     */
    @Override
    public double soma(double x, double y) {
        t = x + y;
        return t;
    }

    @Override
    public double subi(double x, double y) {
        t = x - y;
        return t;
    }

    @Override
    public double muti(double x, double y) {
        t = x * y;
        return t;
    }

    @Override
    public double div(double x, double y) {
        t = x / y;
        return t;
    }

}
