/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tests;

import com.mycompany.calculadora.Met_Calc;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author wilson
 */
public class CalculadoraTest {
    
    /**
     * Test of soma method, of class Met_Calc.
     */
    @Test
    public void testSoma() {
        Met_Calc instance = new Met_Calc();
        System.out.println("soma");
        final double x = 5.0;
        final double y = 4.0;
        final double esp = 9.0;
        final double obti = instance.soma(x, y);      
        assertEquals(esp,obti);        
    }

    /**
     * Test of subi method, of class Met_Calc.
     */
    @Test
    public void testSubi() {
        Met_Calc instance = new Met_Calc();
        System.out.println("subi");
        final double x = 5.0;
        final double y = 4.0;
        final double esp = 1.0;
        final double obti = instance.subi(x, y);
        assertEquals(esp,obti);        
    }

    /**
     * Test of muti method, of class Met_Calc.
     */
    @Test
    public void testMuti() {
        Met_Calc instance = new Met_Calc();
        System.out.println("muti");
        final double x = 5.0;
        final double y = 4.0;
        final double esp = 20.0;
        final double obti = instance.muti(x, y);
        assertEquals(esp,obti);        
    }

    /**
     * Test of div method, of class Met_Calc.
     */
    @Test
    public void testDiv() {
        Met_Calc instance = new Met_Calc();
        System.out.println("div");
        final double x = 5.0;
        final double y = 4.0;
        final double esp = 1.25;
        final double obti = instance.div(x, y);
        assertEquals(esp,obti);        
    }

}